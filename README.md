# Drupal 8 Dev Env
Just for playing around and testing drupal 8.

**Docker image(s) used:**
- (Built) PHP 7.2 + Drupal 8.5.0 https://hub.docker.com/_/drupal/
- (Image) MariaDB 10.3

**Setup and run:**
- $ docker-compose build
- $ docker-compose up -d

Test (default port): http://localhost:3100
Note: first time loading the page may give an error message, that's because the database gets initialised the first time then. Refresh the page once.

**Drupal**
User: Admin
Pass: pass

Docker environment variables:
- HOST_PORT=3100 {number} (optional) Defines Drupal process port
- MYSQL_ROOT_PASSWORD {string} Defines MYSQL Root password
